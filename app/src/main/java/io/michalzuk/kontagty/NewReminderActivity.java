package io.michalzuk.kontagty;

import android.app.AlertDialog;
import android.app.LoaderManager;
import android.content.ContentValues;
import android.content.CursorLoader;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.Loader;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;

import io.michalzuk.kontagty.data.DatabaseData;
import io.michalzuk.kontagty.utils.ReminderDates;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

/**
 * Created by michalzuk on 05/12/17.
 */

public class NewReminderActivity extends AppCompatActivity implements LoaderManager.LoaderCallbacks<Cursor> {

    private static final int EXISTING_REMINDER_LOADER = 0;


    private Toolbar mToolbar;
    private EditText mTextViewImieNazwisko;
    private TextView mTextViewNrTelefonu, mTextViewAdresEmail, mTextViewPlec;
    private Spinner mSpinnerPlec;
    private Calendar mCalendar;
//    private int mYear, mMonth, mHour, mMinute, mDay;
    private String mImieNazwisko;
    private String mNrTelefonu;
    private String mEmail;
    private String mPlec;
    private String mPlec2;
    private String mAvater;
    private String mActive;

    private Uri mCurrentReminderUri;
    private boolean mVehicleHasChanged = false;

    // Values for orientation change
    private static final String KEY_IMIE_NAZWISKO = "imie_nazwisko_key";
    private static final String KEY_NR_TELEFONU = "nr_telefonu_key";
    private static final String KEY_EMAIL = "email_key";
    private static final String KEY_PLEC = "plec_key";
    private static final String KEY_AVATAR = "avatar_key";
    private static final String KEY_ACTIVE = "active_key";


    // Constant values in milliseconds
    private static final long milMinute = 60000L;
    private static final long milHour = 3600000L;
    private static final long milDay = 86400000L;
    private static final long milWeek = 604800000L;
    private static final long milMonth = 2592000000L;

    private View.OnTouchListener mTouchListener = new View.OnTouchListener() {
        @Override
        public boolean onTouch(View view, MotionEvent motionEvent) {
            mVehicleHasChanged = true;
            return false;
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_reminder);










       /*for fill your Spinner*/
        List<String> spinnerArray = new ArrayList<String>();
        spinnerArray.add(" ");
        spinnerArray.add("Kobieta");
        spinnerArray.add("Mężczyzna");
        spinnerArray.add("Agender");
        spinnerArray.add("Androgyne");
        spinnerArray.add("Androgynous");
        spinnerArray.add("Bigender");
        spinnerArray.add("Cis");
        spinnerArray.add("Cisgender");
        spinnerArray.add("Cis Female");
        spinnerArray.add("Cis Male");
        spinnerArray.add("Cis Man");
        spinnerArray.add("Cis Woman");
        spinnerArray.add("Cisgender Female");
        spinnerArray.add("Cisgender Male");
        spinnerArray.add("Cisgender Man");
        spinnerArray.add("Cisgender Woman");
        spinnerArray.add("Female to Male");
        spinnerArray.add("FTM");
        spinnerArray.add("Gender Fluid");
        spinnerArray.add("Gender Nonconforming");
        spinnerArray.add("Gender Questioning");
        spinnerArray.add("Gender Variant");
        spinnerArray.add("Genderqueer");
        spinnerArray.add("Intersex");
        spinnerArray.add("Male to Female");
        spinnerArray.add("MTF");
        spinnerArray.add("Neither");
        spinnerArray.add("Neutrois");
        spinnerArray.add("Non-binary");
        spinnerArray.add("Other");
        spinnerArray.add("Pangender");
        spinnerArray.add("Trans");
        spinnerArray.add("Trans*");
        spinnerArray.add("Trans Female");
        spinnerArray.add("Trans Male");
        spinnerArray.add("Trans* Male");
        spinnerArray.add("Trans Man");
        spinnerArray.add("Trans* Man");
        spinnerArray.add("Trans Person");
        spinnerArray.add("Trans* Person");
        spinnerArray.add("Trans Woman");
        spinnerArray.add("Trans* Woman");
        spinnerArray.add("Transfeminine");
        spinnerArray.add("Transgender");
        spinnerArray.add("Transgender Female");
        spinnerArray.add("Transgender Male");
        spinnerArray.add("Transgender Man");
        spinnerArray.add("Transgender Person");
        spinnerArray.add("Transgender Woman");
        spinnerArray.add("Transmasculine");
        spinnerArray.add("Transsexual");
        spinnerArray.add("Transsexual Female");
        spinnerArray.add("Transsexual Male");
        spinnerArray.add("Transsexual Man");
        spinnerArray.add("Transsexual Person");
        spinnerArray.add("Transsexual Woman");
        spinnerArray.add("Two-Spirit");

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item, spinnerArray);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        Spinner spinner = (Spinner) findViewById(R.id.plec_spinner);
        spinner.setAdapter(adapter);
        final String text = spinner.getSelectedItem().toString();

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view,
                                       int position, long id) {
                Object item = adapterView.getItemAtPosition(position);
                if (item != null || item != " ") {
                    Toast.makeText(NewReminderActivity.this, item.toString(),
                            Toast.LENGTH_SHORT).show();
                }

                String demo = item.toString();
                mPlec2 = demo;

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
                // TODO Auto-generated method stub
                Object item = adapterView.getItemAtPosition(1);
                mPlec2 = item.toString();

            }
        });




















        Intent intent = getIntent();
        mCurrentReminderUri = intent.getData();

        if (mCurrentReminderUri == null) {

            setTitle(getString(R.string.dodaj_szczegoly));
            invalidateOptionsMenu();
        } else {

            setTitle(getString(R.string.edytuj_przypomnienie));


            getLoaderManager().initLoader(EXISTING_REMINDER_LOADER, null, this);
        }


        // Komponenty widoku
        mToolbar = findViewById(R.id.toolbar);
        mTextViewImieNazwisko = findViewById(R.id.reminder_title);
        mTextViewNrTelefonu = findViewById(R.id.reminder_numer);
        mTextViewAdresEmail = findViewById(R.id.reminder_email);
//        mTextViewPlec = ;


        // Initialize default values
        mActive = "false";
        mPlec = text;

        mCalendar = Calendar.getInstance();
//        mHour = mCalendar.get(Calendar.HOUR_OF_DAY);
//        mMinute = mCalendar.get(Calendar.MINUTE);
//        mYear = mCalendar.get(Calendar.YEAR);
//        mMonth = mCalendar.get(Calendar.MONTH) + 1;
//        mDay = mCalendar.get(Calendar.DATE);

//        mDate = mDay + "/" + mMonth + "/" + mYear;
//        mTime = mHour + ":" + mMinute;

        // Tytuł Rimajndera
        mTextViewImieNazwisko.addTextChangedListener(new TextWatcher() {

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                mImieNazwisko = s.toString().trim();
                mTextViewImieNazwisko.setError(null);
            }

            @Override
            public void afterTextChanged(Editable s) {}
        });

        mTextViewNrTelefonu.addTextChangedListener(new TextWatcher() {

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                mNrTelefonu = s.toString().trim();
                mTextViewNrTelefonu.setError(null);
            }

            @Override
            public void afterTextChanged(Editable s) {}
        });

        mTextViewAdresEmail.addTextChangedListener(new TextWatcher() {

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                mEmail = s.toString().trim();
                mTextViewAdresEmail.setError(null);
            }

            @Override
            public void afterTextChanged(Editable s) {}
        });



        // Zapisanie stanu, zabezpiecznie przed rotacją telefonu (refreshem)
        if (savedInstanceState != null) {
            String savedImieNazwisko = savedInstanceState.getString(KEY_IMIE_NAZWISKO);
            mTextViewImieNazwisko.setText(savedImieNazwisko);
            mImieNazwisko = savedImieNazwisko;

            String savedTime = savedInstanceState.getString(KEY_NR_TELEFONU);
            mTextViewNrTelefonu.setText(savedTime);
            mNrTelefonu = savedTime;

            String savedAdresEmail= savedInstanceState.getString(KEY_EMAIL);
            mTextViewAdresEmail.setText(savedAdresEmail);
            mEmail = savedAdresEmail;

//            String savedSex= savedInstanceState.getString(KEY_PLEC);
//            mTextViewAdresEmail.setText(savedSex);
//            mEmail = savedSex;

            mPlec = savedInstanceState.getString(KEY_PLEC);
            mActive = savedInstanceState.getString(KEY_ACTIVE);
        }

        setSupportActionBar(mToolbar);
        getSupportActionBar().setTitle(R.string.dodaj_przypomnienie);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);


    }

    @Override
    protected void onSaveInstanceState (Bundle outState) {
        super.onSaveInstanceState(outState);

        outState.putCharSequence(KEY_IMIE_NAZWISKO, mTextViewImieNazwisko.getText());
        outState.putCharSequence(KEY_NR_TELEFONU, mTextViewNrTelefonu.getText());
        outState.putCharSequence(KEY_EMAIL, mTextViewAdresEmail.getText());
        outState.putCharSequence(KEY_PLEC, mPlec2);
        outState.putCharSequence(KEY_ACTIVE, mActive);
    }

//    public void setTime(View v){
//        Calendar now = Calendar.getInstance();
//        TimePickerDialog tpd = TimePickerDialog.newInstance(
//                this,
//                now.get(Calendar.HOUR_OF_DAY),
//                now.get(Calendar.MINUTE),
//                false
//        );
//        tpd.setThemeDark(false);
//        tpd.show(getFragmentManager(), "Timepickerdialog");
//    }

//    public void setDate(View v){
//        Calendar now = Calendar.getInstance();
//        DatePickerDialog dpd = DatePickerDialog.newInstance(
//                (DatePickerDialog.OnDateSetListener) this,
//                now.get(Calendar.YEAR),
//                now.get(Calendar.MONTH),
//                now.get(Calendar.DAY_OF_MONTH)
//        );
//        dpd.show(getFragmentManager(), "Datepickerdialog");
//    }

//    @Override
//    public void onTimeSet(RadialPickerLayout view, int hourOfDay, int minute) {
//        mHour = hourOfDay;
//        mMinute = minute;
//        if (minute < 10) {
//            mTime = hourOfDay + ":" + "0" + minute;
//        } else {
//            mTime = hourOfDay + ":" + minute;
//        }
//        mTimeText.setText(mTime);
//    }

//    @Override
//    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
//        monthOfYear++;
//        mDay = dayOfMonth;
//        mMonth = monthOfYear;
//        mYear = year;
//        mDate = dayOfMonth + "/" + monthOfYear + "/" + year;
//        mDateText.setText(mDate);
//    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_add_reminder, menu);
        return true;
    }


    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);
        // Nowy reminder = schowaj delete
        if (mCurrentReminderUri == null) {
            MenuItem menuItem = menu.findItem(R.id.discard_reminder);
            menuItem.setVisible(false);
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case R.id.save_reminder:


                if (mTextViewImieNazwisko.getText().toString().length() == 0){
                    mTextViewImieNazwisko.setError("Nazwa Rimajndera nie może być pusta !");
                }

                else {
                    saveReminder();
                    finish();
                }
                return true;
            case R.id.discard_reminder:
                showDeleteConfirmationDialog();
                return true;
            case android.R.id.home:
                if (!mVehicleHasChanged) {
                    NavUtils.navigateUpFromSameTask(NewReminderActivity.this);
                    return true;
                }

                DialogInterface.OnClickListener discardButtonClickListener =
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                NavUtils.navigateUpFromSameTask(NewReminderActivity.this);
                            }
                        };

                showUnsavedChangesDialog(discardButtonClickListener);
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void showUnsavedChangesDialog(
            DialogInterface.OnClickListener discardButtonClickListener) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(R.string.anuluj_dodanie_przypomnienia);
        builder.setPositiveButton(R.string.anuluj, discardButtonClickListener);
        builder.setNegativeButton(R.string.kontynuuj_edycje, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                if (dialog != null) {
                    dialog.dismiss();
                }
            }
        });

        AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }

    private void showDeleteConfirmationDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(R.string.usunac_przypomnienie);
        builder.setPositiveButton(R.string.usun, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                deleteReminder();
            }
        });
        builder.setNegativeButton(R.string.anuluj, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                if (dialog != null) {
                    dialog.dismiss();
                }
            }
        });

        AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }

    private void deleteReminder() {
        if (mCurrentReminderUri != null) {
            int rowsDeleted = getContentResolver().delete(mCurrentReminderUri, null, null);

            if (rowsDeleted == 0) {
                Toast.makeText(this, getString(R.string.blad_usuwania),
                        Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(this, getString(R.string.przypomnienie_usuniete),
                        Toast.LENGTH_SHORT).show();
            }
        }

        finish();
    }

    public void saveReminder(){

        ContentValues values = new ContentValues();


        values.put(DatabaseData.AlarmReminderEntry.KEY_IMIE_NAZWISKO, mImieNazwisko);
        values.put(DatabaseData.AlarmReminderEntry.KEY_NR_TELEFONU, mNrTelefonu);
        values.put(DatabaseData.AlarmReminderEntry.KEY_EMAIL, mEmail);
        values.put(DatabaseData.AlarmReminderEntry.KEY_ACTIVE, mActive);
        values.put(DatabaseData.AlarmReminderEntry.KEY_PLEC, mPlec2);


//        mCalendar.set(Calendar.MONTH, --mMonth);
//        mCalendar.set(Calendar.YEAR, mYear);
//        mCalendar.set(Calendar.DAY_OF_MONTH, mDay);
//        mCalendar.set(Calendar.HOUR_OF_DAY, mHour);
//        mCalendar.set(Calendar.MINUTE, mMinute);
//        mCalendar.set(Calendar.SECOND, 0);

//        long selectedTimestamp =  mCalendar.getTimeInMillis();



        if (mCurrentReminderUri == null) {
            Uri newUri = getContentResolver().insert(DatabaseData.AlarmReminderEntry.CONTENT_URI, values);

            if (newUri == null) {
                Toast.makeText(this, getString(R.string.blad_zapisywanie),
                        Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(this, getString(R.string.przepomnienie_zapisane),
                        Toast.LENGTH_SHORT).show();
            }
        } else {

            int rowsAffected = getContentResolver().update(mCurrentReminderUri, values, null, null);

            if (rowsAffected == 0) {
                Toast.makeText(this, getString(R.string.blad_aktualizacja),
                        Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(this, getString(R.string.przypomnienie_aktualizacja),
                        Toast.LENGTH_SHORT).show();
            }
        }

//        if (mActive.equals("true")) {
//
//                new ReminderDates().setAlarm(getApplicationContext(), selectedTimestamp, mCurrentReminderUri);
//            Toast.makeText(this, "Ustawiono Rimajnder " + selectedTimestamp,
//                    Toast.LENGTH_LONG).show();
//            }


        Toast.makeText(getApplicationContext(), "Zapisano",
                Toast.LENGTH_SHORT).show();

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

    }




    @Override
    public Loader<Cursor> onCreateLoader(int i, Bundle bundle) {

        String[] projection = {
                DatabaseData.AlarmReminderEntry._ID,
                DatabaseData.AlarmReminderEntry.KEY_IMIE_NAZWISKO,
                DatabaseData.AlarmReminderEntry.KEY_NR_TELEFONU,
                DatabaseData.AlarmReminderEntry.KEY_EMAIL,
                DatabaseData.AlarmReminderEntry.KEY_PLEC,
                DatabaseData.AlarmReminderEntry.KEY_ACTIVE,
        };

        return new CursorLoader(this,
                mCurrentReminderUri,
                projection,
                null,
                null,
                null);
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor cursor) {
        if (cursor == null || cursor.getCount() < 1) {
            return;
        }

        if (cursor.moveToFirst()) {
            int imieNazwiskoColumnIndex = cursor.getColumnIndex(DatabaseData.AlarmReminderEntry.KEY_IMIE_NAZWISKO);
            int nrTelefonuColumnIndex = cursor.getColumnIndex(DatabaseData.AlarmReminderEntry.KEY_NR_TELEFONU);
            int adresEmailColumnIndex = cursor.getColumnIndex(DatabaseData.AlarmReminderEntry.KEY_EMAIL);
            int plecColumnIndex = cursor.getColumnIndex(DatabaseData.AlarmReminderEntry.KEY_PLEC);

            String imieNazwisko = cursor.getString(imieNazwiskoColumnIndex);
            String nrTelefonu = cursor.getString(nrTelefonuColumnIndex);
            String adresEmail = cursor.getString(adresEmailColumnIndex);
            String plec = cursor.getString(plecColumnIndex);



            mTextViewImieNazwisko.setText(imieNazwisko);
            mTextViewNrTelefonu.setText(nrTelefonu);
            mTextViewAdresEmail.setText(adresEmail);
//            mTextViewPlec.setText(plec);


        }


    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {

    }

}
