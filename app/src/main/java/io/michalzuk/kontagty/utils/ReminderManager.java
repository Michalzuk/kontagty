package io.michalzuk.kontagty.utils;

import android.app.AlarmManager;
import android.content.Context;

/**
 * Created by michalzuk on 05/12/17.
 */

public class ReminderManager {
    private static final String TAG = ReminderManager.class.getSimpleName();
    private static AlarmManager sAlarmManager;
    public static synchronized void injectAlarmManager(AlarmManager alarmManager) {
        if (sAlarmManager != null) {
            throw new IllegalStateException("Rimajnder już ustawiony");
        }
        sAlarmManager = alarmManager;
    }
    static synchronized AlarmManager getAlarmManager(Context context) {
        if (sAlarmManager == null) {
            sAlarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        }
        return sAlarmManager;
    }
}