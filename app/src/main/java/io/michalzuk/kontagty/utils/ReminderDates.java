package io.michalzuk.kontagty.utils;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.net.Uri;
import android.os.Build;

/**
 * Created by michalzuk on 05/12/17.
 */

public class ReminderDates {


    public void setAlarm(Context context, long alarmTime, Uri reminderTask) {
        AlarmManager manager = ReminderManager.getAlarmManager(context);

        PendingIntent operation =
                ReminderNotification.getReminderPendingIntent(context, reminderTask);


        if (Build.VERSION.SDK_INT >= 23) {

            manager.setExactAndAllowWhileIdle(AlarmManager.RTC_WAKEUP, alarmTime, operation);

        } else if (Build.VERSION.SDK_INT >= 19) {

            manager.setExact(AlarmManager.RTC_WAKEUP, alarmTime, operation);

        } else {

            manager.set(AlarmManager.RTC_WAKEUP, alarmTime, operation);

        }
    }


    public void cancelAlarm(Context context, Uri reminderTask) {
        AlarmManager manager = ReminderManager.getAlarmManager(context);

        PendingIntent operation =
                ReminderNotification.getReminderPendingIntent(context, reminderTask);

        manager.cancel(operation);

    }

}
