package io.michalzuk.kontagty;

import android.content.Context;
import android.database.Cursor;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import com.amulyakhare.textdrawable.TextDrawable;
import com.amulyakhare.textdrawable.util.ColorGenerator;
import io.michalzuk.kontagty.data.DatabaseData;

/**
 * Created by michalzuk on 05/12/17.
 */

public class ReminderCursorAdapter extends CursorAdapter {

    private TextView mTextViewImieNazwisko, mTextViewNrTelefonu, mTextViewAdresEmail, mSpinner;
    private ImageView mActiveImage , mThumbnailImage;
    private ColorGenerator mColorGenerator = ColorGenerator.DEFAULT;
    private TextDrawable mDrawableBuilder;

    public ReminderCursorAdapter(Context context, Cursor c) {
        super(context, c, 0 /* flags */);
    }
    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        return LayoutInflater.from(context).inflate(R.layout.alarm_items, parent, false);
    }

    @Override
    public void bindView(View view, Context context, Cursor cursor) {

        mTextViewImieNazwisko = view.findViewById(R.id.recycle_title);
        mTextViewNrTelefonu= view.findViewById(R.id.recycle_date_time);
        mTextViewAdresEmail = view.findViewById(R.id.recycle_repeat_info);
        mSpinner = view.findViewById(R.id.recycle_sex);
//        mActiveImage = view.findViewById(R.id.active_image);
        mThumbnailImage = view.findViewById(R.id.thumbnail_image);

        int imieNazwiskoColumnIndex = cursor.getColumnIndex(DatabaseData.AlarmReminderEntry.KEY_IMIE_NAZWISKO);
        int nrTelefonuColumnIndex = cursor.getColumnIndex(DatabaseData.AlarmReminderEntry.KEY_NR_TELEFONU);
        int adresEmailColumnIndex = cursor.getColumnIndex(DatabaseData.AlarmReminderEntry.KEY_EMAIL);
        int plecColumnIndex = cursor.getColumnIndex(DatabaseData.AlarmReminderEntry.KEY_PLEC);


        String imieNazwiskoTitle = cursor.getString(imieNazwiskoColumnIndex);
        String nrTelefonuTitle = cursor.getString(nrTelefonuColumnIndex);
        String adresEmailTitle = cursor.getString(adresEmailColumnIndex);
        String plecTitle = cursor.getString(plecColumnIndex);



        setReminderTitle(imieNazwiskoTitle);
        setReminderDateTime(nrTelefonuTitle);
        setAdresEmail(adresEmailTitle);
        setPlec(plecTitle);




    }

    public void setReminderTitle(String title) {
        mTextViewImieNazwisko.setText(title);
        String letter = "A";

        if(title != null && !title.isEmpty()) {
            letter = title.substring(0, 1);
        }

        int color = mColorGenerator.getRandomColor();


        mDrawableBuilder = TextDrawable.builder()
                .buildRound(letter, color);
        mThumbnailImage.setImageDrawable(mDrawableBuilder);
    }

    public void setReminderDateTime(String nrTelefonu) {
        mTextViewNrTelefonu.setText(nrTelefonu);
    }

    public void setAdresEmail(String email) {
        mTextViewAdresEmail.setText(email);
    }

    public void setPlec(String plec) {
        mSpinner.setText(plec);
    }

//    public void setReminderRepeatInfo(String repeat, String repeatNo, String repeatType) {
//        if(repeat.equals("true")){
//            mRepeatInfoText.setText("Powtórz co " + repeatNo + " " + repeatType);
//        }else if (repeat.equals("false")) {
//            mRepeatInfoText.setText("Potwarzanie Wyłączone");
//        }
//    }


}
