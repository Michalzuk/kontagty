package io.michalzuk.kontagty.data;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;



/**
 * Created by michalzuk on 05/12/17.
 */

public class DatabaseHelper extends SQLiteOpenHelper {

    private static final String DATABASE_NAME = "kontagtyDatabase.db";

    private static final int DATABASE_VERSION = 1;

    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        String SQL_CREATE_ALARM_TABLE =  "CREATE TABLE " + DatabaseData.AlarmReminderEntry.TABLE_NAME + " ("
                + DatabaseData.AlarmReminderEntry._ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
                + DatabaseData.AlarmReminderEntry.KEY_IMIE_NAZWISKO + " TEXT, "
                + DatabaseData.AlarmReminderEntry.KEY_NR_TELEFONU + " TEXT, "
                + DatabaseData.AlarmReminderEntry.KEY_EMAIL + " TEXT, "
                + DatabaseData.AlarmReminderEntry.KEY_PLEC + " TEXT, "
//                + DatabaseData.AlarmReminderEntry.KEY_AVATAR + " TEXT, "
                + DatabaseData.AlarmReminderEntry.KEY_ACTIVE + " TEXT " + " );";

        sqLiteDatabase.execSQL(SQL_CREATE_ALARM_TABLE);


    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {

    }
}
