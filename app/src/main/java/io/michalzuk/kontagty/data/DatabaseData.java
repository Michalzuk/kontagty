package io.michalzuk.kontagty.data;



import android.content.ContentResolver;
import android.database.Cursor;
import android.net.Uri;
import android.provider.BaseColumns;

/**
 * Created by michalzuk on 05/12/17.
 */

public class DatabaseData {

    private DatabaseData() {}

    public static final String CONTENT_AUTHORITY = "io.michalzuk.kontagty";

    public static final Uri BASE_CONTENT_URI = Uri.parse("content://" + CONTENT_AUTHORITY);

    public static final String PATH_KONTAGTY = "kontagty-path";

    public static final class AlarmReminderEntry implements BaseColumns {

        public static final Uri CONTENT_URI = Uri.withAppendedPath(BASE_CONTENT_URI, PATH_KONTAGTY);

        public static final String CONTENT_LIST_TYPE =
                ContentResolver.CURSOR_DIR_BASE_TYPE + "/" + CONTENT_AUTHORITY + "/" + PATH_KONTAGTY;

        public static final String CONTENT_ITEM_TYPE =
                ContentResolver.CURSOR_ITEM_BASE_TYPE + "/" + CONTENT_AUTHORITY + "/" + PATH_KONTAGTY;

        public final static String TABLE_NAME = "kontagty";

        public final static String _ID = BaseColumns._ID;

        public static final String KEY_IMIE_NAZWISKO= "imie_nazwisko";
        public static final String KEY_NR_TELEFONU = "nr_telefonu";
        public static final String KEY_EMAIL = "email";
        public static final String KEY_PLEC = "plec";
//        public static final String KEY_AVATAR = "avatar";
        public static final String KEY_ACTIVE = "active";

    }

    public static String getColumnString(Cursor cursor, String columnName) {
        return cursor.getString( cursor.getColumnIndex(columnName) );
    }
}

